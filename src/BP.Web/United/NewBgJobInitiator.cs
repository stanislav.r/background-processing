using System.Threading.Channels;

namespace BP.Web;


class ChannelWrapper<T>
{
    public Channel<T> Channel;
    public int Id;
}
public sealed class NewBgJobInitiatorChannel<T>
    where T : BackgroundJobItem
{
    private const int MaxMessagesInChannel = 100;
    private const int MaxChannels = 100;
    private int _currentChannel = 0;
    private readonly ChannelWrapper<T>[] _channels; // This is basically queue in memory

    public NewBgJobInitiatorChannel()
    {
        var options = new UnboundedChannelOptions()
        {
            SingleWriter = false,
            SingleReader = false
        };
        
        // var options = new BoundedChannelOptions(MaxMessagesInChannel)
        // {
        //     SingleWriter = false,
        //     SingleReader = true
        // };
//        _channels = Channel.CreateUnbounded<T>(options);
        
        _channels = new ChannelWrapper<T>[MaxChannels];

        for (int i = 0; i < MaxChannels; i++)
            _channels[i] = new ChannelWrapper<T>()
            {
                Channel = Channel.CreateUnbounded<T>(options),
                Id = i
            };
    }

    public async Task<bool> QueueJob(T jobItem, CancellationToken ct = default)
    {
        while (await _channels[_currentChannel].Channel.Writer.WaitToWriteAsync(ct) && !ct.IsCancellationRequested)
        {
            if (_channels[_currentChannel].Channel.Writer.TryWrite(jobItem))
            {
                _currentChannel = (_currentChannel + 1) % MaxChannels;
                return true;
            }
        }
        return false;
    }
    
    
    // internal IAsyncEnumerable<T> ReadAllJobsAsync(CancellationToken ct = default) => _channel.Reader.ReadAllAsync(ct);
    internal IList<ChannelWrapper<T>> GetReaders(CancellationToken ct = default)
    {
        return _channels.ToArray();
    }
}