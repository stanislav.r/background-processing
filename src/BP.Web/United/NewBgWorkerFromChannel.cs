using System.Threading.Channels;

namespace BP.Web;


/// <summary>
/// Base worker class for processing jobs from the Channel
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class NewBgWorkerFromChannel<T> : BackgroundService
    where T: BackgroundJobItem
{
    private readonly NewBgJobInitiatorChannel<T> _backgroundJobInitiatorChannel; // Form here we basically read new messages
    private readonly ILogger _logger;

    protected NewBgWorkerFromChannel(
        NewBgJobInitiatorChannel<T> backgroundJobInitiatorChannel,
        ILogger logger)
    {
        _backgroundJobInitiatorChannel = backgroundJobInitiatorChannel;
        _logger = logger;
    }
    /// <summary>
    /// This method should perform processing of one item 
    /// </summary>
    /// <param name="jobItem">Job item that was created by initiator</param>
    /// <param name="cancellationToken">To make cancellation or timeout working token should be processed within this method implementation</param>
    /// <returns></returns>
    protected abstract Task<bool> ProcessJobAsync(
        T jobItem, 
        CancellationToken cancellationToken);
    
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var tasks = new List<Task>();

        foreach (var channelWrapper in _backgroundJobInitiatorChannel.GetReaders())
        {
            tasks.Add(Task.Run(async () =>
            {
                await foreach (var jobItem in channelWrapper.Channel.Reader.ReadAllAsync(stoppingToken))
                {
                    jobItem.Channel = channelWrapper.Id; // Indicates channel that got the job 
                    // using var scope = _serviceProvider.CreateScope(); // For each job we can create new service scope
                    var cancellationTokenSource = new CancellationTokenSource();
                    if (jobItem.TimeoutInterval.TotalSeconds > 0)
                    {
                        cancellationTokenSource.CancelAfter(jobItem.TimeoutInterval);
                    }
                    await ProcessJobAsync(jobItem, cancellationTokenSource.Token);
                }
            }, stoppingToken));
        }
    }
}