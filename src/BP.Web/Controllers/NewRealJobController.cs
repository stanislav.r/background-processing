using Microsoft.AspNetCore.Mvc;

namespace BP.Web.Controllers;

[ApiController]
[Route("[controller]")]
public class NewRealJobController : ControllerBase
{
    private readonly ILogger<NewRealJobController> _logger;
    private readonly NewBgJobInitiatorChannel<RealJobDetails> _realJobInitiator;

    public NewRealJobController(ILogger<NewRealJobController> logger,
        NewBgJobInitiatorChannel<RealJobDetails> realJobInitiator)
    {
        _logger = logger;
        _realJobInitiator = realJobInitiator;
    }

    /// <summary>
    /// Endpoint creates new job for processing
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        // Just initiate job
        for (int i = 0; i < 1000; i++)
        {
            // Thread.Sleep(10);
            var res = await _realJobInitiator.QueueJob(new RealJobDetails()
            {
                SomeMoreDetails = $"Job {i:00000}"
            });    
            _logger.LogInformation($"Job {i:00000} added {res}");
        }
        
        return Ok("Task initiated");
    }
    
    /// <summary>
    /// This job will fail with timeout
    /// </summary>
    /// <returns></returns>
    [HttpGet("longJob")]
    public async Task<IActionResult> LongJob()
    {
        // Just initiate job
        var res = await _realJobInitiator.QueueJob(new RealJobDetails()
        {
            SomeMoreDetails = "Job that should timeout",
            TimeoutInterval = TimeSpan.FromSeconds(4)
        });
        return Ok("Task initiated: " + res);
    }
}