using Microsoft.AspNetCore.Mvc;

namespace BP.Web.Controllers;

[ApiController]
[Route("[controller]")]
public class RealJobController : ControllerBase
{
    private readonly ILogger<RealJobController> _logger;
    private readonly BackgroundJobInitiatorChannel<RealJobDetails> _realJobInitiator;

    public RealJobController(ILogger<RealJobController> logger,
        BackgroundJobInitiatorChannel<RealJobDetails> realJobInitiator)
    {
        _logger = logger;
        _realJobInitiator = realJobInitiator;
    }

    /// <summary>
    /// Endpoint creates new job for processing
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        // Just initiate job
        var res = await _realJobInitiator.QueueJob(new RealJobDetails()
        {
            SomeMoreDetails = "Job should be done within timeout limit"
        });
        return Ok("Task initiated: " + res);
    }
    
    /// <summary>
    /// This job will fail with timeout
    /// </summary>
    /// <returns></returns>
    [HttpGet("longJob")]
    public async Task<IActionResult> LongJob()
    {
        // Just initiate job
        var res = await _realJobInitiator.QueueJob(new RealJobDetails()
        {
            SomeMoreDetails = "Job that should timeout",
            TimeoutInterval = TimeSpan.FromSeconds(4)
        });
        return Ok("Task initiated: " + res);
    }
}