namespace BP.Web;

public class NewRealJobBackgroundWorker : NewBgWorkerFromChannel<RealJobDetails>
{
    private readonly ILogger<NewRealJobBackgroundWorker> _logger;

    public NewRealJobBackgroundWorker(
        NewBgJobInitiatorChannel<RealJobDetails> backgroundJobInitiatorChannel, 
        ILogger<NewRealJobBackgroundWorker> logger) 
        : base(backgroundJobInitiatorChannel, logger)
    {
        _logger = logger;
    }

    protected override async Task<bool> ProcessJobAsync(RealJobDetails jobItem, CancellationToken cancellationToken)
    {
        // Here if needed we can get services from the scope
        // _logger.LogInformation($"Started job. Id: {jobItem.Id}. Details: {jobItem.SomeMoreDetails}");

        await Task.CompletedTask; // Some job
        Thread.Sleep(1000);

        if (cancellationToken.IsCancellationRequested)
            throw new TimeoutException("Job timed out!!! ");

        _logger.LogInformation($"Finished job. Channel: {jobItem.Channel} Details: {jobItem.SomeMoreDetails}");
        return true;
    }
}