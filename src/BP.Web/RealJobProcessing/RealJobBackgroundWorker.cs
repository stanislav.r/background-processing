namespace BP.Web;

public class RealJobBackgroundWorker : BackgroundWorkerFromChannel<RealJobDetails>
{
    private readonly ILogger<RealJobBackgroundWorker> _logger;

    public RealJobBackgroundWorker(
        BackgroundJobInitiatorChannel<RealJobDetails> backgroundJobInitiatorChannel, 
        IServiceProvider serviceProvider, 
        ILogger<RealJobBackgroundWorker> logger) 
        : base(backgroundJobInitiatorChannel, serviceProvider, logger)
    {
        _logger = logger;
    }

    protected override async Task<bool> ProcessJobAsync(IServiceScope scope, 
        RealJobDetails jobItem, 
        CancellationToken cancellationToken)
    {
        // Here if needed we can get services from the scope
        // _logger.LogInformation($"Started job. Id: {jobItem.Id}. Details: {jobItem.SomeMoreDetails}");

        await Task.CompletedTask; // Some job
        Thread.Sleep(7000);

        if (cancellationToken.IsCancellationRequested)
            throw new TimeoutException("Job timed out!!! ");
        
        
        _logger.LogInformation($"Finished job. Id: {jobItem.Id} Details: {jobItem.SomeMoreDetails}");
        return true;
    }
}