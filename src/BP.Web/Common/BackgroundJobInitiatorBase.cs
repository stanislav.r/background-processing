namespace BP.Web;

/// <summary>
/// This base abstraction for any type of jobs initiators.
/// Here we can add common validation logic or store job status (e.g. in database), etc.
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class BackgroundJobInitiatorBase<T>
    where T : BackgroundJobItem

{
    private readonly IServiceProvider _serviceProvider;

    protected BackgroundJobInitiatorBase(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }
    
    public abstract Task<bool> QueueJob(T jobItem, CancellationToken ct = default);
}