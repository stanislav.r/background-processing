namespace BP.Web;

/// <summary>
/// Every jobs item details should be inherited from this class
/// </summary>
public abstract class BackgroundJobItem
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public TimeSpan TimeoutInterval { get; set; } = new TimeSpan(0, 5, 0); // Default 5 minute timeout unless changed

    public int Channel;

}