namespace BP.Web;

/// <summary>
/// Common class for worker for processing jobs.
/// </summary>
/// <typeparam name="T"></typeparam>
public abstract class BackgroundWorkerBase<T> : BackgroundService
    where T: BackgroundJobItem
{
    private readonly IServiceProvider _serviceProvider;
    private readonly ILogger<BackgroundWorkerBase<T>> _logger;

    protected BackgroundWorkerBase(IServiceProvider serviceProvider,
        ILogger<BackgroundWorkerBase<T>> logger)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
    }
    
    /// <summary>
    /// This method should perform processing of the one item 
    /// </summary>
    /// <param name="scope">Each job item gets it's own service scope</param>
    /// <param name="jobItem">Job item that was created by initiator</param>
    /// <param name="cancellationToken">To make cancellation or timeout working token should be processed within this method implementation</param>
    /// <returns></returns>
    protected abstract Task<bool> ProcessJobAsync(
        IServiceScope scope, 
        T jobItem, 
        CancellationToken cancellationToken);

    protected virtual async Task ProcessJobInternal(T jobItem)
    {
        using var scope = _serviceProvider.CreateScope(); // For each job we create new service scope
        var cancellationTokenSource = new CancellationTokenSource();
        if (jobItem.TimeoutInterval.TotalSeconds > 0)
        {
            cancellationTokenSource.CancelAfter(jobItem.TimeoutInterval);
        }
        await ProcessJobAsync(scope, jobItem, cancellationTokenSource.Token);
    }
}