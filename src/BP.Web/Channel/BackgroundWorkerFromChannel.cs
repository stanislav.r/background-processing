namespace BP.Web;

public abstract class BackgroundWorkerFromChannel<T> : BackgroundWorkerBase<T>
    where T : BackgroundJobItem
{
    private readonly BackgroundJobInitiatorChannel<T> _backgroundJobInitiatorChannel;

    protected BackgroundWorkerFromChannel(
        BackgroundJobInitiatorChannel<T> backgroundJobInitiatorChannel,
        IServiceProvider serviceProvider, 
        ILogger<BackgroundWorkerFromChannel<T>> logger) 
        : base(serviceProvider,logger)
    {
        _backgroundJobInitiatorChannel = backgroundJobInitiatorChannel;
    }
    
    protected abstract override Task<bool> ProcessJobAsync(IServiceScope scope, T jobItem, 
        CancellationToken cancellationToken);

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        await foreach (var jobItem in _backgroundJobInitiatorChannel.ReadAllJobsAsync(stoppingToken))
        {
            await ProcessJobInternal(jobItem);
        }
    }
}