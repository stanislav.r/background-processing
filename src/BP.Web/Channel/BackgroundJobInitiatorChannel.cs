using System.Threading.Channels;

namespace BP.Web;

/// <summary>
/// This is Channel specific job initiator. Channel is a way to queue jobs within the web server
/// </summary>
/// <typeparam name="T"></typeparam>
public sealed class BackgroundJobInitiatorChannel<T>: BackgroundJobInitiatorBase<T> 
    where T : BackgroundJobItem
{
    private const int MaxMessagesInChannel = 100;
    private readonly Channel<T> _channel;

    public BackgroundJobInitiatorChannel(IServiceProvider serviceProvider) 
        : base(serviceProvider)
    {
        var options = new BoundedChannelOptions(MaxMessagesInChannel)
        {
            SingleWriter = false,
            SingleReader = true
        };
        _channel = Channel.CreateBounded<T>(options);
    }

    public override async Task<bool> QueueJob(T jobItem, CancellationToken ct = default)
    {
        while (await _channel.Writer.WaitToWriteAsync(ct) && !ct.IsCancellationRequested)
        {
            if (_channel.Writer.TryWrite(jobItem))
            {
                return true;
            }
        }
        return false;
    }

    internal IAsyncEnumerable<T> ReadAllJobsAsync(CancellationToken ct = default) => _channel.Reader.ReadAllAsync(ct);
}