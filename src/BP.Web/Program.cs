using BP.Web;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// With inheritance
builder.Services.AddSingleton<BackgroundJobInitiatorChannel<RealJobDetails>>();
builder.Services.AddHostedService<RealJobBackgroundWorker>();

// Without inheritance
builder.Services.AddSingleton<NewBgJobInitiatorChannel<RealJobDetails>>();
builder.Services.AddHostedService<NewRealJobBackgroundWorker>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();